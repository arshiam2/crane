const fs = require("fs");
const fetch = require("node-fetch");
let digits = [];

function stream_pairs(name, ip) {
  var readStream = fs.createReadStream("randomNumbers.txt", "utf8");
  readStream
    .on("data", function(chunk) {
      digits = chunk.split("\n");
      digits.map(digit => {
        body = { action: "END", state: [name, digit] };
        fetch("http://" + ip + ":3000/queue-add", {
          method: "post",
          body: JSON.stringify(body),
          headers: { "Content-Type": "application/json" }
        });
      });
    })
    .on("end", function() {
      console.log("done");
    });
}

function map_add_hi(string, ip) {
  stream_pairs(string[0], ip);
  return [];
}

function resolver(data, ip) {
  switch (data.action) {
    case "START":
      return map_add_hi(data.state, ip);
    default:
      return { action: data.action, state: "NONE" };
  }
}

module.exports = resolver;
