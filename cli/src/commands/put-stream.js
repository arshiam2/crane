// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");
var csv = require("csv-stream");
const fetch = require("node-fetch");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "put-stream [name]";
module.exports.describe = "Writes localfilename to the SDFS as sdfsfilename.";
module.exports.builder = (yargs: any) => yargs;

const machineToIps = {
  "1": "172.22.156.15",
  "2": "172.22.158.15",
  "3": "172.22.154.16",
  "4": "172.22.156.16",
  "5": "172.22.158.16",
  "6": "172.22.154.17",
  "8": "172.22.158.17",
  "9": "172.22.154.18",
  "10": "172.22.156.18"
};

queue = [];

let i = 0;

var options = {
  delimiter: "\t"
};

function parseTwitterTweets(data) {
  let parsed =
    data[
      "#image_id,unixtime,rawtime,title,total_votes,reddit_id,number_of_upvotes,subreddit,number_of_downvotes,localtime,score,number_of_comments,username"
    ];
  return parsed.split(",")[3];
}

async function getCSVdata(source) {
  if (source == "reddit") {
    fs.createReadStream("redditSubmissions.csv")
      .pipe(csv.createStream(options))
      .on("data", function(data) {
        let message = parseTwitterTweets(data);
        const body = { action: "START", state: message };
        queue.push(body);
      });
  }
  if (source == "movies") {
    fs.createReadStream("movies.txt")
      .pipe(csv.createStream(options))
      .on("data", function(data) {
        data = Object.values(data)[0].split(",");
        if (data.length == 2) {
          const body = {
            action: "START",
            state: {
              movie: data[0].replace("product/productId: ", ""),
              score: data[1].replace("review/score: ", "")
            }
          };
          queue.push(body);
        }
      });
  }

  if (source == "food") {
    let profileName = "";
    fs.createReadStream("finefoods.txt")
      .pipe(csv.createStream(options))
      .on("data", function(data) {
        data = Object.values(data)[0];
        if (data.startsWith("review/profileName:")) {
          profileName = data.replace("review/profileName:", "");
        }
        if (data.startsWith("review/score:")) {
          const body = {
            action: "START",
            state: [profileName, data.replace("review/score:", "")]
          };
          queue.push(body);
        }
      });
  }
}

module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster();
  console.log("Starting to stream data to spout");


  await getCSVdata(argv.name);

  ips = [];
  const machines = Object.keys(machineToIps);
  await Promise.all(
    machines.map(async machine => {
      var url = "http://" + machineToIps[machine] + ":3000/master/";
      try {
        let response = await fetch(url);
        let parsed = await response.text();
        if (parsed === "Alive") {
          ips.push(Number(machine));
        }
      } catch (e) {}
    })
  );
  const sendTopology = ips.map(number => {
    var target =
      "http://" + machineToIps[number] + ":3000/setTopology/" + argv.name;
    request.post(target, function(error, response, body) {
    })
  });

  async se

  setInterval(function sync() {
    if (queue.length > 0) {
      const body = queue.pop();
      getMaster().then(m => {
        console.log(m)
        master = m
      })
      // console.log(master)
      var target = "http://" + master + ":3000/spout";
      fetch(target, {
        method: "post",
        body: JSON.stringify(body),
        headers: { "Content-Type": "application/json" }
      }).catch(err => (x = 0));
      if (i % 1000 == 0) {
        console.log(i);
      }
      i += 10;
    } else {
      console.log("Finished streamming data to the spout");
      process.exit();
    }
  }, 2);
});
