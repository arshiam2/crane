// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");
var csv = require("csv-stream");
const fetch = require("node-fetch");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "get-stream";
module.exports.describe = "Writes localfilename to the SDFS as sdfsfilename.";
module.exports.builder = (yargs: any) => yargs;



module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster();
  setInterval(function sync() {
      var target = "http://"+ master + ":3000/sink/";
      fetch(target)
        .then(res => (res.text()))
        .then(data => {
          if (data == "EMPTY") {
            process.exit()
          } else {
            console.log(data);
          }
        });
  }, 25);
  
});
