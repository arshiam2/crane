// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "get-second";
module.exports.describe = "Writes the names of the latest numversions of sdfsfilename to a localfilename.";
module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster()
  var target = "http://"+ master + ":3000/getSecond/";
  console.log(target)
  request.post(target, function(error, response, body) {
      console.log(body.toString())
    })
});
