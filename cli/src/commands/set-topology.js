// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "set-topology [number]";
module.exports.describe =
  "Writes the names of the latest numversions of sdfsfilename to a localfilename.";
module.exports.builder = (yargs: any) => yargs;

const machineToIps = {
    "1": "172.22.156.15",
    "2": "172.22.158.15",
    "3": "172.22.154.16",
    "4": "172.22.156.16",
    "5": "172.22.158.16",
    "6": "172.22.154.17",
    "8": "172.22.158.17",
    "9": "172.22.154.18",
    "10": "172.22.156.18"
  };

module.exports.handler = handleErrors(async (argv: {}) => {
  const master = await getMaster();
  ips = [master];
  ips.map(number => {
    var target = "http://" + number + ":3000/setTopology/" + argv.number;
    request.post(target, function(error, response, body) {
      console.log(body);
    });
  });
});
