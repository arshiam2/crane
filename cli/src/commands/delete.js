// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");

module.exports.command = "delete [sdfsfilename]";
module.exports.describe =
  "Removes sdfsfilename and all it’s versions from the SDFS.";
module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
  master = await getMaster()
  var target =
    "http://" +
    master +
    ":3000/delete/" +
    argv.sdfsfilename;
  request.post(target);
});
