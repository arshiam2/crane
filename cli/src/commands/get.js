// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var express = require("express");
var http = require("http");
var request = require("request");

const handleErrors = require("../utils/handleErrors");
const getMaster = require("../utils/getMaster");


var app = express();

app.post("/upload/:filename/", function(req, res) {
    // console.log("GETTING THIS")
    var filename = path.basename(req.params.filename);
    filename = path.resolve(process.cwd() + "/", filename);
  
    var dst = fs.createWriteStream(filename);
    req.pipe(dst);
    dst.on("drain", function() {
      console.log("drain", new Date());
      req.resume();
    });
    req.on("end", function() {
      res.sendStatus(200);
      process.exit(0);
    });
});

module.exports.command = "get [sdfsfilename] [localfilename]";
module.exports.describe = "Reads sdfsfilename from the SDFS to localfilename.";
module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
  http.createServer(app).listen(3000, "172.16.244.74", function() {
    console.log("Express server listening on port 3000");
  });

  const master = await getMaster()

  var target = "http://"+master+":3000/get/" + argv.sdfsfilename +  "/" + argv.localfilename;
  var ws = request.post(target);
});
