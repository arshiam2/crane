let fetch = require("node-fetch")

const machineToIps = {
    "1": "172.22.156.15",
    "2": "172.22.158.15",
    "3": "172.22.154.16",
    "4": "172.22.156.16",
    "5": "172.22.158.16",
    "6": "172.22.154.17",
    "8": "172.22.158.17",
    "9": "172.22.154.18",
    "10": "172.22.156.18"
  };

module.exports = async function getMaster() {
    machinesUp = [];
    const machines = Object.keys(machineToIps);
    await Promise.all(
      machines.map(async machine => {
        var url = "http://" + machineToIps[machine] + ":3000/master/";
        try {
          let response = await fetch(url);
          let parsed = await response.text();
          if (parsed === "Alive") {
            machinesUp.push(Number(machine));
          }
        } catch (e) {}
      })
    );
    master = 0;
    if (machinesUp.length > 0) {
      master = Math.min(...machinesUp);
    }
    console.log("Master is " + master)
    return machineToIps[String(master)]
  }